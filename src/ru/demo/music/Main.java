package ru.demo.music;

import java.io.*;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Класс для чтения и скачивания музыки
 */

public class Main {

    private static final String IN_FILE_TXT = "inFile.txt";
    private static final String OUT_FILE_TXT = "outFile.txt";
    private static final String PATH_TO_MUSIC = "C:\\Users\\User\\Desktop\\";
    private static final String symbol = "(?<=data-url=\")[^\"]*(?=\")";
    /**
     * Метод main.
     */
    public static void main(String[] args) {
        read();
        download();
    }
    /**
     * Метод для скачивания.
     */
    private static void download() {
        try (BufferedReader musicFile = new BufferedReader(new FileReader(OUT_FILE_TXT))) {//создание объекта и запись содержимого файла с расширение txt в этот объект.
            String music;
            int count = 0;
            try {
                while ((music = musicFile.readLine()) != null) {
                    Upload down = new Upload(music, PATH_TO_MUSIC + String.valueOf(count) + ".mp3");
                    down.start();
                    count++;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * Метод для чтения и записи Url для скачивания.
     */
    private static void read() {
        String Url;
        try (BufferedReader inFile = new BufferedReader(new FileReader(IN_FILE_TXT));
             BufferedWriter outFile = new BufferedWriter(new FileWriter(OUT_FILE_TXT))) {
            while ((Url = inFile.readLine()) != null) {
                String result = thread(Url);
                url(outFile, result);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * Метод для чтения коды страницы с которой скачиваем песни.
     */
    private static String thread(String Url) throws IOException {
        URL url = new URL(Url);

        String result;
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(url.openStream()))) {//открытие потока
            result = bufferedReader.lines().collect(Collectors.joining("\n"));//Код страницы
        }
        return result;
    }
    /**
     * Метод для записи url для скачивания в текстовой документ.
     */
    private static void url(BufferedWriter outFile, String result) throws IOException {
        Pattern pattern_link = Pattern.compile(symbol);
        Matcher matcher = pattern_link.matcher(result);
        int i = 0;
        while (matcher.find() && i < 1) {//озвращает true ,если ссылка совпадает с шаблоном.
            outFile.write(matcher.group() + "\r\n");
            i++;
        }
    }
}

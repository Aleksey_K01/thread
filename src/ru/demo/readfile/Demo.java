package ru.demo.readfile;
import java.io.*;
import java.io.IOException;
import java.util.concurrent.Semaphore;

public class Demo {
    public static void main(String[] args) throws InterruptedException, IOException {
        try (DataInputStream input1 = new DataInputStream(new FileInputStream("input1.txt"))) {
            try (DataInputStream input2 = new DataInputStream(new FileInputStream("input4.txt"))) {
                try (DataOutputStream output = new DataOutputStream(new FileOutputStream("output.txt"))) {
                    Semaphore sem = new Semaphore(1);

                    ReadFile thread1 = new ReadFile(input1, output, sem);
                    ReadFile thread2 = new ReadFile(input2, output, sem);


                    thread1.start();
                    thread2.start();


                    thread1.join();
                    thread2.join();

                }
            }
        }

    }

}

package ru.demo.readfile;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.concurrent.Semaphore;

public class ReadFile extends Thread {
    Semaphore sem;
    private DataInputStream input;
    private  DataOutputStream output;

    public ReadFile(DataInputStream input, DataOutputStream output, Semaphore sem) {
        this.input = input;
        this.output = output;
        this.sem = sem;
    }

    public void run() {
        String str;
        try {
            while (input.available() != 0) {
                try {
                    sem.acquire();
                    str = input.readLine();
                    output.writeChars(str + "\n");
                    sem.release();
                    Thread.currentThread().sleep(10);
                } catch (InterruptedException ex) {
                }
            }

        } catch (IOException ex) {


        }
    }
}


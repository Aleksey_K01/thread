package ru.demo.runexample;

public class RandomRunExample extends Thread {
    public void run(){
        //получение имени текущего потока
        System.out.println(Thread.currentThread().getName());
    }
    /**
     * Cоздание и запуск 10 потоков
     */
    public static void example(){
        for(int i=0;i < 10;i++){
            Thread thread = new RandomRunExample();
            thread.start();
        }
    }
}

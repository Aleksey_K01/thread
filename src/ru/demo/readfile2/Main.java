package ru.demo.readfile2;

import java.io.IOException;
/**
 * Основной класс позволяющий запустить чтение из файлов и копирование  в файил.
 * Тут присутсвует последовательное и параллельное  копирование.
 * Происходит открытие файла input4.txt и последующие копирование в файлы output5.txt и output7.txt.
 * А также вывод выполнения операции копирования милесекундах.
 *
 *  final long before1 = System.currentTimeMillis();
 thread1.start();
 thread1.join();
 final long after1 = System.currentTimeMillis();
 System.out.println("Время потока = " + (after1 -
 before1));
 long y = after1 - before1;

 thread2.start();
 thread2.join();
 final long after2 = System.currentTimeMillis();
 System.out.println("Время потока = " + (after2 -
 before1));
 long u = after2 - before1;

 System.out.println("Суммарное время потоков = " + (y + u));

 }
 }
 */

public class Main {
        public static void main(String[] args) throws InterruptedException, IOException {
            ReadFile2 thread1 = new ReadFile2("input4.txt", "output5.txt");
            ReadFile2 thread2 = new ReadFile2("input4.txt", "output7.txt");
            final long before = System.currentTimeMillis();
            thread1.start();
            thread2.start();
            final long after1 = System.currentTimeMillis();
            System.out.println("Время поока после запуска = " + (after1 - before));
            thread1.join();
            thread2.join();
            final long after = System.currentTimeMillis();
            System.out.println("Время потока по завершению = " + (after - before));
        }
    }


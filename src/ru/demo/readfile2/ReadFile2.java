package ru.demo.readfile2;

import java.io.*;
/**
 * Класс ReadFile2 расширяющий класс Thread.
 * Класс в котором создаются переменные для чтения и записи.
 */

 class ReadFile2 extends Thread {
        private String input;
        private String output;

    /**
     * Конструктор по умолчанию.
     * @param input входные данные.
     * @param output выходные данные.
     */
        ReadFile2(String input, String output) {
            this.input = input;
            this.output = output;
        }

    /**
     * Метод run позволяющий читать и копировать посторочно.
     */
        public void run() {
            String str;
            try {
                BufferedReader input = new BufferedReader(new FileReader(this.input));
                BufferedWriter output = new BufferedWriter(new FileWriter(this.output));

                while (input.read() !=-1) {
                    str = input.readLine();
                    output.write(str + "\n");
                }
                output.flush();
            } catch (IOException ignored) {
            }
        }
    }
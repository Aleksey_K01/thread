package ru.demo.threadPrior;

/**
 * Класс демонстрирующий поведение потоков
 */
public class ThreadPrior extends Thread {
    public static void main(String[] args) throws InterruptedException {
        Thread thread = Thread.currentThread();
        thread.setPriority(5);

        Thread thread2 = new ThreadPrior();

        thread2.setPriority(10);
        int a = 1;
        thread2.start();
        for (int i = 1; i <= 200; i++) {
            System.out.println("1 поток с приор "
                    + thread.getPriority() + " -" + i);
            Thread.sleep(a);
        }
    }




        public void run()  {
            Thread thread = Thread.currentThread();
            int a = 1;
                for(int i= 1; i <=200; i++) {
                    System.out.println("2 поток с приор "
                            + thread.getPriority() + " -" + i);
                    try {
                        Thread.sleep(a);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

            }
        }


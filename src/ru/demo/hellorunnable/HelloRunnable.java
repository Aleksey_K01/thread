package ru.demo.hellorunnable;


/**
 * Реализация интерфейса Runnable
 * предназначенный для размещения кода,исполняемого в потоке.
 * Runnable-объект пересылается в конструктор Thread
 * и с помощью метода start() поток запускается
 */


public class HelloRunnable implements  Runnable {
    public void run() {
        for (int i = 5; i > 0; i--) {
            System.out.println(i);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                System.out.println("Поток завершен");
                System.out.println("Hello from a thread");
            }


        }
    }

   public void RunTheThread(){
       (new Thread(new Thread())).start();
       System.out.println("i new potok");
   }
    public static void main(String[] args) {
        (new Thread(new HelloRunnable())).start();
        for (int i = 5; i > 0; i--) {
            System.out.println(i);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                System.out.println("Поток завершен");
                System.out.println("Hello from main thread");
            }

        }

    }


}
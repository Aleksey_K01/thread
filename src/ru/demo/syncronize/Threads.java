package ru.demo.syncronize;

public class Threads extends Thread {
    public volatile static int i = 0;

    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            add();
            System.out.println(Threads.i + " - "+getName());
        }
    }
    private static synchronized void add(){
        i++;

    }
    public static void main(String[] args) {
        Threads threads = new Threads();
        Threads threads1 = new Threads();
        threads.setName("1 поток");
        threads1.setName("2 поток");
        threads.start();
        threads1.start();
    }
}
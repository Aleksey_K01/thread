package ru.demo.music;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

/**
 * Скачивание файлов с помощью потоков
 */
public class Upload extends Thread {
    private String strUrl;
    private String filepath;

    /**
     * Конструктор с параметром
     * @param strUrl
     * @param filepath
     */
    Upload(String strUrl, String filepath) {
        this.strUrl = strUrl;
        this.filepath = filepath;
    }

    /**
     * Скачивается музыка
     */
    public void run() {
        try {
            URL url = new URL(strUrl);
            ReadableByteChannel byteChannel = Channels.newChannel(url.openStream());
            FileOutputStream stream = new FileOutputStream(filepath);
            stream.getChannel().transferFrom(byteChannel, 0, Long.MAX_VALUE);
            stream.close();
            byteChannel.close();
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }
}


package ru.demo.interferens;

import java.util.concurrent.atomic.AtomicInteger;

public class InterferenceExample {
    private static final int HUNDRED_MILLION =100_000_000;
    private AtomicInteger counter =new AtomicInteger();

    public boolean stop() {
        return counter.incrementAndGet()>HUNDRED_MILLION;
    }
    public  void  examle() throws InterruptedException {
        InterferenceThread thread1= new InterferenceThread("Поток 1",this);
        InterferenceThread thread2= new InterferenceThread("Поток 2",this);
        thread1.start();
        thread2.start();
        thread1.join();
        thread2.join();
        System.out.println("Ожидаем: " +HUNDRED_MILLION);
        System.out.println("Получили:" +thread1.getI());
    }

    public void example() {

    }
    }


